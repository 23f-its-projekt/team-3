# team 3 

## Problemformulering

I denne rapport belyses følgende problemstilling:

Hvordan kan man forbedre et forretnings’ sikkerhed, mod Privilege Escalation?

Hvordan kan man identificere Privilege Escalation?
Hvordan kan man identificere sårbarheder, der kan føre til Privilege Escalation?
Hvilke foranstaltninger kan man tage for at mitigere disse sårbarheder?

## Afgrænsning

Rapporten tager afsæt i et netværksdiagram, som er udarbejdet med Unik System Design A/S. i Rapporten dokumenteres udvalgte sårbarheder der kan føre til Privilege Escalation samt sikkerhedsforanstaltninger der kan implementeres, for at mitigere disse sårbarheder. De udvalgte sårbarheder er ikke udtømmende for hvad der kan føre til Privilege Escalation, ligeledes er vores forslag til foranstaltninger kun en måde at gribe det an på.

## Tilgangsvinkler (skal måske slettes fra Rapport?)

By implementing proper OS role management we improve the security of our systems and network.
Golden Standard
By using IPS and IDS systems we can look after errors where our OS roles are not held.
Proper Log Management can help detect where attacks originate from


# Baggrund

Vi vil samarbejde med virksomheden Unik System Design A/S 

# Formål

At udarbejde et netværks diagram med overblik over processer, aktiver og services inde for Uniks netværk.

# Mål

At ud fra det netværks diagram vi har udarbejdet med Unik, at opfylde store dele af læringsmålene vi har indefor fagene systemsikkerhed, netværks- og kommunikationssikkerhed og intro til itsikkerhed.

## Need to have

1. Et fyldestgørende netværks diagram
2. Risiko vurdering / interessentanalyse
3. En virkende teknisk sikkerheds tiltag
4. ...

## Nice to have

1. Virkende Netbox basseret på det udarbejde netværks diagram
2. Et virkende eksempel på et IDS system.
3. Et virkende eksempel på et IPS system.
4. ...

# Tidsplan

## uge 8
- projektide
- projektplan

## uge 11
- Snak med Unik om mulighed for samarbejde

## uge 12
- Find en dato for team møde med UNIk

## uge 13
- Måske møde

## uge 16
- Forløbig indholdsfortegnelse: https://docs.google.com/document/d/1cGbTuZG0qIyhbi2K43YM0lNOQfyVUSY5G30WGyg1F88/edit?usp=sharing

## uge 18
- Møde med lærene
- Indledning: https://docs.google.com/document/d/1PV5qg0eoNlOFKTyTfUXgnXivOXA7zkh-6JrDZUstVm4/edit#heading=h.58c9wk8o8p13

## uge 23
- eksamen 19+20 juni

# Organisation

Hvem er medlemmerne i teamet og hvad er deres roller i projektet.  
Magnus
Aubrey
Aynur

# Forventningsafstemning

I gruppen er vi enige i følgende punkter:
- Vi giver besked til hinanden i godt tid om hvornår vi mødes (altså ikke en time før mødetidspunkt, medmindre det er online)

- Alt kommunikation foregår primært på discord.

- Vi skal være gode til at spørge om hjælp. Vi skal ikke sidde med det samme i alt for lang tid. Det samme gælder hvis det vi troede var en let opgave, er en svær og eller lang opgave.

- Meld sygdom til resten af gruppen.

- Når projektet går i gang, skal vi mødes mindst en gang om ugen.


# Risikostyring

Hvilke risici er der ifht. projektets fuldførelse og hvordan skal de håndteres hvis de opstår?

Dette punkt kræver en **realistisk** vurdering af hvad der kan gå galt i projektet samet hvilke tiltage teamet beslutter skal udføres hvis den enkelte risici opstår.

Her bør også angives hvilken metode der anvendes til risiko styring. 

Inspiration kan hentes her [https://www.cfl.dk/artikler/risikoanalyse](https://www.cfl.dk/artikler/risikoanalyse)

# Interessenter


# Opsætning af netværket
Da vi havde lavet vores netværksdiagram, havde vi regnet med at hver maskine skulle kobles til hver deres vmnet. da vi så gik igang med at opsætte det, kunne vi ikke få dem til at snakke sammen, når de ikke var på det samme net, og vi besluttede os for at fortsætte med at have dem på det samme net.



# kommunikation

Hvordan er det besluttet at kommunikere i projektet og er der ud fra interessant samt risiko analysen behov for at etablere kommunikation der understøtter dem.

## Kommunikationskanaler

- gitlab issue board
- element
- discord
- email adresser
- mm.

## Kommunikationsaktiviteter

- Møder
- Periodiske emails
- Branching strategier
- Kommunikation med eksterne
- mm.


# Perspektiv

Projektet danner grundlag for uddannelsens øvrige semesterprojekter og skal opfattes som en øvelse i at lave et godt bachelor projekt som afslutning på uddannelsen.

# Evaluering

Hvordan evalueres projektet udover de obligatoriske eksamens afleveringer.  
Det anbefales at evaluere alle projekter ved afslutning for at synliggøre og samle erfaringer til brug i næste projekt.  
Det er også en god ide at, som team, at kigge tilbage på projektets forløb for at reflektere over hvad de enekelte teammedlemmer har lært i løbet af projektet.

Endelig bør et godt udført og veldokumenteret projekt inkluderes i en studerendes portfolio/dokumentation side så det er muligt for andre at få glæde af projeket.

# Referencer

Relevante overordnede referencer til essentielle ting der benyttes i projektet, hver reference bør have en beskrivelse der kort begrunder og forklarer hvordan referencer er relevant for projektet.

- Links til dokumentation
- Links til metoder
- mm.

# Links
- https://esdhweb.ucl.dk/D22-2039642.pdf
